﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MRVTestesAutomatizados.Enums;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System.Diagnostics;
using OpenQA.Selenium.Interactions;

namespace MRVTestesAutomatizados.Pages
{
    public class PageBase
    {
        private int TempoEsperaEmSegundos = 120;
        struct TiposInput
        {
            public const string TipoSubmit = "submit";
            public const string TipoButton = "button";
        }

        struct NomeBotoes
        {
            public const string NOVA_RESERVA = "Nova Reserva";
        }

        struct IdBotoesReserva
        {
            public const string ID_NOVA_RESERVA = "ContentPlaceHolder1_btnNovaReserva";
        }


        public void ClicarEmElemento(IWebDriver webDriver, By by)
        {
            var elemento = EncontrarElementoBy(webDriver, by);
            elemento.Click();
        }

        public void PreencherTextBox(IWebDriver webDriver, string texto, By by)
        {
            var elementoTextBox = EncontrarElementoBy(webDriver, by);
            elementoTextBox.Clear();
            elementoTextBox.SendKeys(texto);
        }

        public void SelecionarComboBox(IWebDriver webDriver, string value,
            By by, Seletores.SeletorComboBox seletorCombo)
        {
            var elementoComboBox = new SelectElement(EncontrarElementoBy(webDriver, by));

            switch (seletorCombo)
            {
                case Seletores.SeletorComboBox.ByIndex:
                    var index = 0;
                    if (Int32.TryParse(value, out index))
                    {
                        elementoComboBox.SelectByIndex(index);
                    }
                    break;
                case Seletores.SeletorComboBox.ByText:
                    elementoComboBox.SelectByText(value);
                    break;
                case Seletores.SeletorComboBox.ByValue:
                    elementoComboBox.SelectByValue(value);
                    break;
                default:
                    throw new Exception("Seletor de combobox não existente.");
            }


        }

        public IWebElement EncontrarElementoBy(IWebDriver webDriver, By by)
        {
            try
            {
                EsperarPorElemento(webDriver, by, TempoEsperaEmSegundos);
                return webDriver.FindElement(by);
            }
            catch (NoSuchElementException)
            {
                Thread.Sleep(TimeSpan.FromSeconds(TempoEsperaEmSegundos));
                return EncontrarElementoBy(webDriver, by);
            }

        }

        public void ClicoBotaoDescricao(IWebDriver driver, string descricao)
        {
            var botao = EncontrarBotaoPorValue(driver, descricao);

            if (botao != null)
            {
                try
                {
                    botao.Click();
                }
                catch (InvalidOperationException)
                {
                    if (botao != null)
                    {
                        var idBotao = botao.GetAttribute("id");
                        ((IJavaScriptExecutor)driver).ExecuteScript("$('#"+idBotao.ToString()+"').click()", botao);
                    }
                }
                catch(ElementNotVisibleException)
                {
                    if (botao != null)
                    {
                        var idBotao = botao.GetAttribute("id");
                        ((IJavaScriptExecutor)driver).ExecuteScript("$('#" + idBotao.ToString() + "').click()", botao);
                    }
                }
            }
            else
            {
                ClicarBotaoNome(driver, descricao);
            }
        }

        private void ClicarBotaoNome(IWebDriver webDriver, string descricao)
        {
            if (NomeBotoes.NOVA_RESERVA.Equals(descricao))
            {
                ClicarEmElemento(webDriver, By.Id(IdBotoesReserva.ID_NOVA_RESERVA));
            }

        }

        public IWebElement EncontrarBotaoPorValue(IWebDriver driver, string value)
        {
            var inputsTela = driver.FindElements(By.TagName("input"));

            foreach (var input in inputsTela)
            {
                try
                {
                    if (input.GetAttribute("value") != null)
                    {

                        if ((TiposInput.TipoSubmit.Equals(input.GetAttribute("type").ToLower()) || TiposInput.TipoButton.Equals(input.GetAttribute("type").ToLower()))
                            && (value.ToLower().Equals(input.GetAttribute("value").ToLower())) || value.ToLower().Equals(input.Text.ToLower()))
                        {
                            if (input.Enabled && input.Displayed)
                            {
                                return input;
                            }
                        }
                    }
                }
                catch (StaleElementReferenceException)
                {
                    return null;
                }
            }


            StackFrame frame = new StackFrame(1);
            var method = frame.GetMethod();
            var type = method.DeclaringType;
            var name = method.Name;

            if (name != null && name.Equals("EncontrarBotaoPorValue"))
            {
                var botao = EncontrarElementoPorTexto(driver, value);
                return botao != null ? botao : null;
            }

            Thread.Sleep(TimeSpan.FromSeconds(30));
            EncontrarBotaoPorValue(driver, value);

            return null;
        }

        public virtual void ValidaExibicaoMensagem(IWebDriver driver, string mensagem)
        {
            Thread.Sleep(TimeSpan.FromSeconds(10));
            Assert.IsTrue(driver.PageSource.Contains(mensagem));
        }

        public virtual void ValidaNaoExibicaoMensagem(IWebDriver driver, string mensagem)
        {
            Assert.IsFalse(driver.PageSource.Contains(mensagem));
        }

        public virtual void ValidaExibicaoMensagem(IWebDriver driver, string mensagem, By seletorMensagem)
        {
            EsperarPorElemento(driver, seletorMensagem, 120);
            Assert.IsTrue(driver.PageSource.Contains(mensagem));
        }

        internal void AceitarCancelarAlert(IWebDriver driver, bool acao)
        {
            if (acao)
            {
                driver.SwitchTo().Alert().Accept();
            }
            else
            {
                driver.SwitchTo().Alert().Dismiss();
            }

        }

        internal void EsperarPorElemento(IWebDriver driver, By seletor, int tempoSegundos)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(TempoEsperaEmSegundos));
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(seletor));
            wait.Until(ExpectedConditions.ElementToBeClickable(seletor));
        }

        internal IWebElement EncontrarElementoPorTexto(IWebDriver driver, string texto)
        {
            try
            {
                return driver.FindElement(By.XPath("//button[text() = "+ texto +")]"));
            }
            catch (NotFoundException)
            {
                return null;
            }
        }

        public bool isAlertPresent(IWebDriver driver)
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        public bool isElementVisible(IWebDriver driver, By by)
        {
            return driver.FindElement(by).Displayed;
        }

        public void EsperarElementoNaoEstarVisivel(IWebDriver driver, By seletor)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(TempoEsperaEmSegundos));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(seletor));
        }
    }
}
