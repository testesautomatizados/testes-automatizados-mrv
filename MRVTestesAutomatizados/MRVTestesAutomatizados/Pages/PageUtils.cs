﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Pages
{
    public class PageUtils: PageBase
    {
        struct Ids
        {
            public const string Empreendimento = "ContentPlaceHolder1_txtLookupEmpreendimento";
            public const string ImgBuscaEmpreendimento = "ContentPlaceHolder1_imgLookupEmpreendimento";
            public const string BtnPesquisarProduto = "ContentPlaceHolder1_btnPesquisar";
            public const string IdTabelaProduto = "#ContentPlaceHolder1_dgProduto";
        }

        struct SeletorCss
        {
            public const string TabelaEmpreendimentos = "#ContentPlaceHolder1_dvModalPopupLookupFiltros > table:nth-child(5)";
            public const string TabelaProdutos = "#ContentPlaceHolder1_dgProduto";
            public const string RodapeTabelaProdutos = "#ContentPlaceHolder1_dgProduto > tbody > tr.textogridPager > td > table";
        }

        

        public void SelecionarProdutoPorNumeroReservaEmpreendimento(IWebDriver driver, String nomeEmpreendimento)
        {
            PreencherTextBox(driver, nomeEmpreendimento, By.Id(Ids.Empreendimento));
            ClicarEmElemento(driver, By.Id(Ids.ImgBuscaEmpreendimento));

            var tabelaEmpreendimento = EncontrarElementoBy(driver, By.CssSelector(SeletorCss.TabelaEmpreendimentos));
            SelecionarEmpreendimentoPorNomeEmpreendimento(tabelaEmpreendimento, nomeEmpreendimento);

            ClicarEmElemento(driver, By.Id(Ids.BtnPesquisarProduto));

            var tabelaProduto = EncontrarElementoBy(driver, By.CssSelector(SeletorCss.TabelaProdutos));
            SelecionarProdutoSemReserva(driver, tabelaProduto);


        }

        private void SelecionarProdutoSemReserva(IWebDriver driver, IWebElement tabelaProduto)
        {
            var linhasTabela = tabelaProduto.FindElements(By.TagName("tr"));
            foreach (var linha in linhasTabela)
            {
                var colunas = linha.FindElements(By.TagName("td"));
                if (colunas.Count == 9)
                {
                    if (colunas[2].Text == "0" && colunas[6].Text == "Não")
                    {
                        colunas[0].Click();
                        return;
                    }
                }
            }
            // Alterar Paginação.
        }


        private void SelecionarEmpreendimentoPorNomeEmpreendimento(IWebElement tabelaEmpreendimento, string nomeEmpreendimento)
        {
            var linhasTabela = tabelaEmpreendimento.FindElements(By.TagName("tr"));
            foreach (var linha in linhasTabela)
            {
                var colunas = linha.FindElements(By.TagName("td"));
                foreach (var coluna in colunas)
                {
                    if (coluna.Text.Equals(nomeEmpreendimento))
                    {
                        colunas[1].Click();
                        return;
                    } 
                }
            }


        }

        private bool IsElementPresent(IWebDriver driver,By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

    }
}
