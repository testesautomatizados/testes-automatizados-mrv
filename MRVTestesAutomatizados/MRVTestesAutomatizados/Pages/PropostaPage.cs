﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace MRVTestesAutomatizados.Pages
{
    public class PropostaPage : PageBase
    {
        struct SeletoresCamposProposta
        {
            public const string SELETOR_RENDA_PRESUMIDA = "#ContentPlaceHolderMasterPageResponsive_txtRendaPresumida";
            public const string SELETOR_FINANCIAMENTO = "#ContentPlaceHolderMasterPageResponsive_txtFinanciamento";
            public const string SELETOR_FGTS = "#ContentPlaceHolderMasterPageResponsive_txtFGTS";
            public const string SELETOR_TIPO_PAGAMENTO_SINAL = "#ContentPlaceHolderMasterPageResponsive_ddlTipoPagamento";
            public const string SELETOR_VALOR_SINAL = "#ContentPlaceHolderMasterPageResponsive_txtValor";
            public const string SELETOR_VENCIMENTO_SINAL = "#ContentPlaceHolderMasterPageResponsive_txtDataVencimento";
            public const string SELETOR_SALDO_DISTRIBUIR = "#ContentPlaceHolderMasterPageResponsive_txtSaldoFinalaSerDistribuidoDesconto";
            public const string SELETOR_TIPO_PAGAMENTO_REGISTRO = "#ContentPlaceHolderMasterPageResponsive_ddlFormaPagamentoServicoRegistroContrato";
            public const string SELETOR_NOME_PRODUTO_CANCELAMENTO = "#ContentPlaceHolder1_txtProdutoFiltro";
        }

        struct SeletoresValoresTabelaVendas
        {
            public const string SELETOR_FINANCIAMENTO_TABELA_VENDAS = "#ContentPlaceHolderMasterPageResponsive_spanTabelaVendasTotalFinanciamento";
            public const string SELETOR_FGTS_TABELA_VENDAS = "";
            public const string SELETOR_SINAL_TABELA_VENDAS = "#ContentPlaceHolderMasterPageResponsive_spanTabelaVendasTotalSinal";
        }

        struct IdsBotoesProposta
        {
            public const string BOTAO_APAGAR_RESERVA = "ContentPlaceHolder1_dgReserva_imgCancelar_0";
            public const string ID_BOTAO_ADICIONAR_SINAL = "imbDaAdicionarDivisaoSinal";
        }

        public const string ID_DIV_MENSAGEM_RESERVA = "pnlMensagemInt";
        public const string ID_BOTAO_FECHAR_MENSAGEM = "lnkFechar";


        internal void InformoValorTabelaVendasFinanciamentoComoFinanciamentoProposta(IWebDriver driver)
        {
            var valorFinancimamentoTabelaVendas = EncontrarElementoBy(driver, By.CssSelector(SeletoresValoresTabelaVendas.SELETOR_FINANCIAMENTO_TABELA_VENDAS));
            PreencherTextBox(driver, valorFinancimamentoTabelaVendas.Text, By.CssSelector(SeletoresCamposProposta.SELETOR_FINANCIAMENTO));
        }

        public void ValidaExibicaoMensagem(IWebDriver driver)
        {
            EsperarPorElemento(driver, By.Id(ID_DIV_MENSAGEM_RESERVA), 120);
            ClicarEmElemento(driver, By.Id(ID_BOTAO_FECHAR_MENSAGEM));
        }

        internal void InformoSinalProposta(IWebDriver driver, double valorSinal)
        {
            PreencherTextBox(driver, valorSinal.ToString(), By.CssSelector(SeletoresCamposProposta.SELETOR_VALOR_SINAL));

            SelecionarComboBox(driver, "CC - MRV", By.CssSelector(SeletoresCamposProposta.SELETOR_TIPO_PAGAMENTO_SINAL), Enums.Seletores.SeletorComboBox.ByText);

            PreencherTextBox(driver, DateTime.Today.AddDays(2).Date.ToString("dd/MM/yyyy"), By.CssSelector(SeletoresCamposProposta.SELETOR_VENCIMENTO_SINAL));

            ClicarEmElemento(driver, By.Id(IdsBotoesProposta.ID_BOTAO_ADICIONAR_SINAL));
        }

        internal void InformoSaldoDistribuirFgst(IWebDriver driver)
        {
            var saldoDistribuir = EncontrarElementoBy(driver, By.CssSelector(SeletoresCamposProposta.SELETOR_SALDO_DISTRIBUIR));
            PreencherTextBox(driver, saldoDistribuir.GetAttribute("value").ToString(), By.CssSelector(SeletoresCamposProposta.SELETOR_FGTS));
        }

        internal void InformarRendaPresumidaProposta(IWebDriver driver, double rendaPresumida)
        {
            PreencherTextBox(driver, rendaPresumida.ToString(), By.CssSelector(SeletoresCamposProposta.SELETOR_RENDA_PRESUMIDA));
        }

        internal void InformoFormaPagamentoRegistroContrato(IWebDriver driver, string formaPagamentoRegistro)
        {
            SelecionarComboBox(driver, "À vista", By.CssSelector(SeletoresCamposProposta.SELETOR_TIPO_PAGAMENTO_REGISTRO), Enums.Seletores.SeletorComboBox.ByText);
        }
    }
}
