﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MRVTestesAutomatizados.Enums;
using OpenQA.Selenium;

namespace MRVTestesAutomatizados.Pages
{
    public class LoginPage : PageBase
    {
        private struct Xpaths
        {
            public const string XPATH_TEXTBOX_USUARIO = "//*[@id='txtLogin']";
            public const string XPATH_TEXTBOX_SENHA = "//*[@id='txtSenha']";
            public const string XPATH_BOTAO_ENTRAR = "//*[@id='btnLogar']";
        }

        public void FazerLogin(IWebDriver driver, string usuario, string senha)
        {
            PreencherTextBox(driver, usuario, By.XPath(Xpaths.XPATH_TEXTBOX_USUARIO));
            PreencherTextBox(driver, senha, By.XPath(Xpaths.XPATH_TEXTBOX_SENHA));
            ClicarEmElemento(driver, By.XPath(Xpaths.XPATH_BOTAO_ENTRAR));
        }

        public void PreencherTextBoxUsuario (IWebDriver driver, string usuario )
        {
            PreencherTextBox(driver, usuario,By.XPath(Xpaths.XPATH_TEXTBOX_USUARIO));
        }

        public void PreencherTextBoxSenha (IWebDriver driver, string senha )
        {
            PreencherTextBox(driver, senha,By.XPath(Xpaths.XPATH_TEXTBOX_SENHA));
        }

        public void ClicarBotaoEntrar (IWebDriver driver)
        {
            ClicarEmElemento(driver, By.XPath(Xpaths.XPATH_BOTAO_ENTRAR));
        }


    }
}
