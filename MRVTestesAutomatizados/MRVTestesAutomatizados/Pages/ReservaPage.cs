﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MRVTestesAutomatizados.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Pages
{
    public class ReservaPage : PageBase
    {
        struct IdsBotoesReserva
        {
            public const string BOTAO_NOVA_PROPOSTA = "ContentPlaceHolder1_btnNovaReserva";
            public const string BOTAO_OK_DATA_BASE = "ContentPlaceHolder1_btnOk";
            public const string BOTAO_PESQUISA_EMPREENDIMENTO = "ContentPlaceHolder1_imgLookupEmpreendimento";
            public const string BOTAO_SELECIONA_EMPREENDIMENTO = "ContentPlaceHolder1_dgGridLookupFiltros_imgSelectLookup_0";
            public const string BOTAO_PESQUISAR_PRODUTO = "ContentPlaceHolder1_btnPesquisar";
            public const string BOTAO_VINCULAR_PRODUTO = "ContentPlaceHolder1_dgProduto_imgVincular_0";
            public const string BOTAO_PESQUISAR_CLIENTE = "ContentPlaceHolder1_btnLookupPesquisaCliente";
            public const string BOTAO_VINCULAR_CLIENTE = "ContentPlaceHolder1_dgLookup_imgSelectLookup_0";
            public const string BOTAO_VINCULAR_ATENDIMENTO = "ContentPlaceHolder1_ucModalPesquisaAtendimentosID_dgAtendimentos_imgSelecionar_0";
            public const string ID_NOVA_RESERVA = "ContentPlaceHolder1_btnNovaReserva";
            public const string ID_BOTAO_FECHAR_CAIXA_MENSAGEM = "lnkFechar";
        }

        struct IdsCamposReserva
        {
            public const string CAMPO_EMPREENDIMENTO = "ContentPlaceHolder1_txtLookupEmpreendimento";
            public const string CAMPO_PRODUTO = "ContentPlaceHolder1_txtProduto";
            public const string CAMPO_CLIENTE = "ContentPlaceHolder1_txtCliente";
            public const string CAMPO_TIPO_FINANCIAMENTO = "ContentPlaceHolder1_ddltipoFinanciamento";
            public const string CAMPO_BANCO_DESLIGAMENTO = "ContentPlaceHolder1_ddlBancoDesligamento";
        }

        struct IdsDivsReserva
        {
            public const string DIV_MENSAGEM_RESERVA = "pnlMensagemInt";
        }

        internal void InformarProduto(IWebDriver driver, string produto)
        {
            PreencherTextBox(driver, produto, By.Id(IdsCamposReserva.CAMPO_PRODUTO));
        }

        internal void PesquisarProduto(IWebDriver driver, string produto)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.BOTAO_PESQUISAR_PRODUTO));
        }

        internal void InformarCliente(IWebDriver driver, string cliente)
        {
            PreencherTextBox(driver, cliente, By.Id(IdsCamposReserva.CAMPO_CLIENTE));
        }

        internal void VincularCliente(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.BOTAO_VINCULAR_CLIENTE));
        }

        internal void PesquisarCliente(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.BOTAO_PESQUISAR_CLIENTE));
        }

        internal void FecharCaixaMensagem(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.ID_BOTAO_FECHAR_CAIXA_MENSAGEM));
        }

        internal void VincularAtendimentoCliente(IWebDriver driver)
        {
            var botaoVincularAtendimento = EncontrarElementoBy(driver, By.Id(IdsBotoesReserva.BOTAO_VINCULAR_ATENDIMENTO));
            ((IJavaScriptExecutor)driver).ExecuteScript("$('#ContentPlaceHolder1_ucModalPesquisaAtendimentosID_dgAtendimentos_imgSelecionar_0').click()", botaoVincularAtendimento);
        }

        public void ConfirmarDataBase(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.BOTAO_OK_DATA_BASE));
        }

        internal void InformarTipoFinanciamento(IWebDriver driver, string tipoFinanciamento)
        {
            SelecionarComboBox(driver, tipoFinanciamento, By.Id(IdsCamposReserva.CAMPO_TIPO_FINANCIAMENTO), Seletores.SeletorComboBox.ByText);
        }

        internal void InformarBancoDesligamento(IWebDriver driver, string bancoDesligamento)
        {
            SelecionarComboBox(driver, bancoDesligamento, By.Id(IdsCamposReserva.CAMPO_BANCO_DESLIGAMENTO), Seletores.SeletorComboBox.ByText);
        }

        internal void InformarEmpreendimento(IWebDriver driver, string empreendimento)
        {
            PreencherTextBox(driver, empreendimento, By.Id(IdsCamposReserva.CAMPO_EMPREENDIMENTO));
        }

        internal void VincularEmpreendimento(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.BOTAO_SELECIONA_EMPREENDIMENTO));
        }

        internal void SelecionarEmpreendimento(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.BOTAO_PESQUISA_EMPREENDIMENTO));
           // var botaoPesquisa = EncontrarElementoBy(driver, By.Id(IdsBotoesReserva.BOTAO_PESQUISA_EMPREENDIMENTO));
           // botaoPesquisa.SendKeys("");
        }

        internal void ConfirmarGeracaoReserva(IWebDriver driver)
        {
            AceitarCancelarAlert(driver, true);
        }

        internal void VincularProduto(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdsBotoesReserva.BOTAO_VINCULAR_PRODUTO));
        }

        internal void ClicoBotaoNovaReserva(IWebDriver driver)
        {
            var botaoNovaReserva = EncontrarElementoBy(driver, By.Id(IdsBotoesReserva.ID_NOVA_RESERVA));
            
        }

        public override void ValidaExibicaoMensagem(IWebDriver driver, string mensagem)
        {
            EsperarPorElemento(driver, By.Id(IdsDivsReserva.DIV_MENSAGEM_RESERVA), 120);
            Assert.IsTrue(driver.PageSource.Contains(mensagem));
        }

    }
}
