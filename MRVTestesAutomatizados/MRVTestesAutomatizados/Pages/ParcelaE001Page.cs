﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Pages
{
    [Binding]
    public class ParcelaE001Page : PageBase
    {
        private ReservaPage _reservaPage = new ReservaPage();
        private ReservaPage ReservaPage
        {
            get
            {
                return _reservaPage ?? new ReservaPage();
            }
        }

        private int mesBaseContrato = DateTime.Now.Month;

        struct SeletorCampos
        {
            public const string CAMPO_VALOR_SINAL = "#ContentPlaceHolderMasterPageResponsive_txtValor";
            public const string COMBO_TIPO_PAGAMENTO_SINAL = "#ContentPlaceHolderMasterPageResponsive_ddlTipoPagamento";
            public const string CAMPO_VENCIMENTO_SINAL = "#ContentPlaceHolderMasterPageResponsive_txtDataVencimento";
            public const string CAMPO_NOME_PRODUTO = "#ContentPlaceHolder1_txtProdutoFiltro";
            public const string BOTAO_CONFIRMAR = "#ContentPlaceHolder1_dgGridLookupFiltros_imgSelectLookup_0";
            public const string BOTAO_PESQUISA = "#ContentPlaceHolder1_btnPesquisaFiltroLst";
            public const string BOTAO_PESQUISA_PRODUTO = "#ContentPlaceHolder1_ibtnLookupProdutoFiltro";
            public const string CONFIRMA_PRODUTO = "#ContentPlaceHolder1_dgGridLookupFiltros_imgSelectLookup_0";
            public const string BOTAO_OK_DATA_BASE = "ContentPlaceHolder1_btnOk";
            public const string CAMPO_DATA_BASE = "#ContentPlaceHolder1_txtDataBase";
            public const string DIA_DATA_BASE = "#ContentPlaceHolder1_CalendarDataInicial_day_0_5";
            public const string FECHAR_ALERTA = "#lnkFechar";
        }

        struct IdCampos
        {
            public const string BOTAO_ADICIONAR_SINAL = "imbDaAdicionarDivisaoSinal";
            public const string BOTAO_APAGAR_RESERVA = "ContentPlaceHolder1_dgReserva_imgCancelar_0"; 
        }

        internal void InformarNomeProdutoParaCancelamento(IWebDriver driver, string nomeProduto)
        {
            PreencherTextBox(driver, nomeProduto, By.CssSelector(SeletorCampos.CAMPO_NOME_PRODUTO));
        }

        internal void ApagarReserva(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.Id(IdCampos.BOTAO_APAGAR_RESERVA));
        }

        internal void ConfirmarProduto(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.CssSelector(SeletorCampos.BOTAO_CONFIRMAR));
        }

        internal void PesquisarReservas(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.CssSelector(SeletorCampos.BOTAO_PESQUISA_PRODUTO));
            Thread.Sleep(60);
            ClicarEmElemento(driver, By.CssSelector(SeletorCampos.CONFIRMA_PRODUTO));
        }

        internal void RealizarPesquisa(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.CssSelector(SeletorCampos.BOTAO_PESQUISA));
        }

        internal void ConfirmarExclusao(IWebDriver driver)
        {
            AceitarCancelarAlert(driver, true);
        }

        internal void ConfirmarDataBase(IWebDriver driver, string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                var vetorData = data.Split('/');
                this.mesBaseContrato = int.Parse(vetorData[1]);
                PreencherTextBox(driver, data.Replace("/", ""), By.CssSelector(SeletorCampos.CAMPO_DATA_BASE));
            }
            ReservaPage.ConfirmarDataBase(driver);
        }

        internal void FehcarAlerta(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.CssSelector(SeletorCampos.FECHAR_ALERTA));
        }

        internal void InformoSinalProposta(IWebDriver driver, double valorSinal, string tipoPagamento, DateTime dataSinal)
        {
            PreencherTextBox(driver, valorSinal.ToString(), By.CssSelector(SeletorCampos.CAMPO_VALOR_SINAL));

            SelecionarComboBox(driver, tipoPagamento, By.CssSelector(SeletorCampos.COMBO_TIPO_PAGAMENTO_SINAL), Enums.Seletores.SeletorComboBox.ByText);

            PreencherDataSinal(driver, dataSinal);

            ClicarEmElemento(driver, By.Id(IdCampos.BOTAO_ADICIONAR_SINAL));
        }

        private void PreencherDataSinal(IWebDriver driver, DateTime dataSinal)
        {
            var mesesAdicionar = mesBaseContrato - dataSinal.Month;

            if (dataSinal.Day <= 17)
            {
                PreencherTextBox(driver, dataSinal.AddDays(3).AddMonths(mesesAdicionar).ToString("dd/MM/yyyy"), By.CssSelector(SeletorCampos.CAMPO_VENCIMENTO_SINAL));
            }
            else
            {
                PreencherTextBox(driver, dataSinal.AddDays(2).AddMonths(mesesAdicionar).ToString("dd/MM/yyyy"), By.CssSelector(SeletorCampos.CAMPO_VENCIMENTO_SINAL));
            }
        }
    }
}
