﻿using MRVTestesAutomatizados.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Pages

{
    public class MenuPage : PageBase
    {
        private IWebDriver driver;

        public MenuPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        private struct MenuSistemasXpaths
        {
            public const string XPATH_MENU_SISTEMA_CORRETOR = "//*[@id='ucPrincipal_liMRVCorretor']";
            public const string XPATH_MENU_SISTEMA_VALIDADOR = "//*[@id='ucPrincipal_lbtnValidadorPropostas']";
            public const string XPATH_MENU_SISTEMA_PROSPECT = "//*[@id='ucPrincipal_lbtnProspect']";
            public const string XPATH_MENU_SISTEMA_NOTICIAS = "//*[@id='ucPrincipal_lbtnNoticia']";
            public const string XPATH_MENU_SISTEMA_CONFIGURACOES = "//*[@id='ucPrincipal_txtConfiguracao']";
            public const string XPATH_MENU_SISTEMA_COMISSAO = "";
        }

        private struct SistemaCorretorSubMenusXpaths
        {
            public const string XPATH_SUBMENU_CLIENTE = "//*[@id='ucMenuCorretor_txtSessaoCliente']";
            public const string XPATH_SUBMENU_CREDITO = "//*[@id='ucMenuCorretor_txtSessaoCredito']";
            public const string XPATH_SUBMENU_RESERVA = "//*[@id='ucMenuCorretor_txtSessaoReservas']";
            public const string XPATH_SUBMENU_LINKS_UTEIS = "//*[@id='ucMenuCorretor_txtLinksUteis']";
            public const string XPATH_SUBMENU_RELATORIOS = "//*[@id='ucMenuCorretor_txtRelatorios']";
            public const string XPATH_SUBMENU_PAGAMENTO_RPA = "//*[@id='ucMenuCorretor_txtRPA']";
        }

        struct Seletores
        {
            public const string SUBMENU_RESERVA_TELARESERVA = "#ucMenuCorretor_sessaoReservas";
            public const string SUBMENU_RESERVA_HOME = "#ucMenuCorretor_sessaoReservas";
        }

        public const string ID_CARREGAMENTO_NOTICIAS = "noticiasLoading";
        public const string TELA_RESERVA = "Tela Reserva";
        public const string TELA_RESERVA_HOME = "Reserva Home";

        public void SelecionarMenuSistema(Sistemas.MenuSistema enumSitemas)
        {
            switch (enumSitemas)
            {
                case Sistemas.MenuSistema.Corretor:
                    SelecionarMenu(MenuSistemasXpaths.XPATH_MENU_SISTEMA_CORRETOR);
                    break;
                case Sistemas.MenuSistema.Validador:
                    SelecionarMenu(MenuSistemasXpaths.XPATH_MENU_SISTEMA_VALIDADOR);
                    break;
                case Sistemas.MenuSistema.Prospect:
                    SelecionarMenu(MenuSistemasXpaths.XPATH_MENU_SISTEMA_PROSPECT);
                    break;
                case Sistemas.MenuSistema.Noticias:
                    SelecionarMenu(MenuSistemasXpaths.XPATH_MENU_SISTEMA_NOTICIAS);
                    break;
                case Sistemas.MenuSistema.Configuracoes:
                    SelecionarMenu(MenuSistemasXpaths.XPATH_MENU_SISTEMA_CONFIGURACOES);
                    break;
            }
        }

        private void SelecionarMenu(string xPathMenu)
        {
            EsperarElementoNaoEstarVisivel(driver, By.Id(ID_CARREGAMENTO_NOTICIAS));
            ClicarEmElemento(driver, By.XPath(xPathMenu));
        }

        public void SelecionarSubMenu(Sistemas.SubMenuCorretor subMenu, string origem)
        {
            switch (subMenu)
            {
                case Sistemas.SubMenuCorretor.Cliente:
                    SelecionarMenu(SistemaCorretorSubMenusXpaths.XPATH_SUBMENU_CLIENTE);
                    break;
                case Sistemas.SubMenuCorretor.Credito:
                    SelecionarMenu(SistemaCorretorSubMenusXpaths.XPATH_SUBMENU_CREDITO);
                    break;
                case Sistemas.SubMenuCorretor.LinksUteis:
                    SelecionarMenu(SistemaCorretorSubMenusXpaths.XPATH_SUBMENU_LINKS_UTEIS);
                    break;
                case Sistemas.SubMenuCorretor.PagamentoRPA:
                    SelecionarMenu(SistemaCorretorSubMenusXpaths.XPATH_SUBMENU_PAGAMENTO_RPA);
                    break;
                case Sistemas.SubMenuCorretor.Relatorios:
                    SelecionarMenu(SistemaCorretorSubMenusXpaths.XPATH_SUBMENU_RELATORIOS);
                    break;
                case Sistemas.SubMenuCorretor.Reservas:
                    if (origem.Equals(TELA_RESERVA))
                    {
                        ClicarEmElemento(driver,By.XPath(SistemaCorretorSubMenusXpaths.XPATH_SUBMENU_RESERVA));
                    }
                    else if (origem.Equals(TELA_RESERVA_HOME))
                    {
                        ClicarEmElemento(driver, By.CssSelector(Seletores.SUBMENU_RESERVA_HOME));
                    }
                    else
                    {
                        isElementVisible(driver, By.XPath("//*[@id='noticiasLoading']/p/img"));// verificar
                        SelecionarMenu(SistemaCorretorSubMenusXpaths.XPATH_SUBMENU_RESERVA);
                    }
                    break;
            }
        }
    }
}
