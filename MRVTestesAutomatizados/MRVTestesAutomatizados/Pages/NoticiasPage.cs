﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Pages
{
    public class NoticiasPage : PageBase
    {
        private IWebDriver webDriver;

        public NoticiasPage(IWebDriver driver)
        {
            this.webDriver = driver;
        }

        private struct Xpaths
        {
            public const string XPATH_MENU_CORRETOR = "//*[@id='ucPrincipal_liMRVCorretor']";
            public const string XPATH_MENU_RESERVA = "//*[@id='ucMenuCorretor_sessaoReservas']";
        }

        public void ClicarMenuSistemaCorretor()
        {
            ClicarEmElemento(webDriver, (By.XPath(Xpaths.XPATH_MENU_CORRETOR)));
        }

        public void ClicarMenuReserva()
        {
            ClicarEmElemento(webDriver, By.XPath(Xpaths.XPATH_MENU_RESERVA));
        }

    }
}
