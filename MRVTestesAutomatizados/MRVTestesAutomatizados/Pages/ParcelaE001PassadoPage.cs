﻿using MRVTestesAutomatizados.Enums;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Pages
{
    public class ParcelaE001PassadoPage : PageBase
    {
        struct MensagensErro
        {
            public const string TESTE = "";
        }

        struct SeletoresCSS{
            public const string CAMPO_STATUS = "#ContentPlaceHolder1_ddlStatusReservaFiltro";
            public const string BOTAO_GERAR_PROPOSTA = "#ContentPlaceHolder1_dgReserva_imgProposta_0";
        }

        internal void PesquisarStatus(IWebDriver driver, string status)
        {
            SelecionarComboBox(driver, status, By.CssSelector(SeletoresCSS.CAMPO_STATUS), Seletores.SeletorComboBox.ByText);
        }

        internal void GerarProposta(IWebDriver driver)
        {
            ClicarEmElemento(driver, By.CssSelector(SeletoresCSS.BOTAO_GERAR_PROPOSTA));
        }
    }
}
