﻿Feature: ParcelaE001
	Com intuito de evitar impactos nas regras de vencimento do sinal e testar a funcionalidade 
	eu como usuário dos dos sitemas comerciais 
	desejo informar diferentes formas de vencimento de sinais.

@E001_Erros
Scenario: Validar vencimento de parcela E001 na data base do mês futuro. (erro)
	Given Fazer login "ademarrosa" e "ademar@1234"
	And Eu clico no menu "MRVCorretor"
	And Eu clico no SubMenu "Reservas" na tela "Tela principal"
	And Eu Clico no botão "Nova Reserva"
	And Eu informo a data base "05/06/2017"
	And Informo o empreendimento "PARQUE SERRA AZUL"
	And Informo o Produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Informo o cliente "TESTE AUTOMATIZADOS"
	And Seleciono o atendimento correspondente ao cliente
	And Informo o Tipo de Financiamento "CEF ASSOCIATIVO P IPCA +1% (D90)"
	And Seleciono o Banco de Desligamento do Cliente "CAIXA ECONÔMICA FEDERAL"
	And Eu Clico no botão "Salvar"
	And Confirmo a geração da reserva
	And Exibe a mensagem "Reserva gerada com sucesso!"
	And Eu clico no botão "Gerar Proposta"
	And Informo uma Renda Presumida de 15000,00
	And Informo o valor da tabela de vendas de financiamento como financiamento
	And Informo um Valor de Sinal de "10000" Tipo de pagamento "BL - Boleto" data de vencimento do sinal "06/17/2017"
	And Valido se a mensagem "Ato (E001) - Vencimento fora do período permitido. Favor selecionar um vencimento de 2 dia(s) a partir do dia da data atual e mês/ano da data base do contrato." está na tela
	And Eu clico no SubMenu "Reservas" na tela "Tela Reserva"
	And Eu informo o produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Eu pesquiso as reservas
	And Eu realizo a pesquisa
	And Eu apago a reserva do produto selecionado
	When Confirmo a remoção
	Then Fecho o alerta

Scenario: Validar vencimento de parcela E001 na data base do mes atual. (erro)
	Given Eu Clico no botão "Nova Reserva"
	And Eu Clico no botão "Nova Reserva"
	And Eu informo a data base ""
	And Informo o empreendimento "PARQUE SERRA AZUL"
	And Informo o Produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Informo o cliente "TESTE AUTOMATIZADOS"
	And Seleciono o atendimento correspondente ao cliente
	And Informo o Tipo de Financiamento "CEF ASSOCIATIVO P IPCA +1% (D90)"
	And Seleciono o Banco de Desligamento do Cliente "CAIXA ECONÔMICA FEDERAL"
	And Eu Clico no botão "Salvar"
	And Confirmo a geração da reserva
	And Exibe a mensagem "Reserva gerada com sucesso!"
	And Eu clico no botão "Gerar Proposta"
	And Informo uma Renda Presumida de 15000,00
	And Informo o valor da tabela de vendas de financiamento como financiamento
	And Informo um Valor de Sinal de "10000" Tipo de pagamento "BL - Boleto" data de vencimento do sinal "06/21/2017"
	And Valido se a mensagem "Ato (E001) - Vencimento fora do período permitido. Favor selecionar um vencimento de 2 dia(s) a partir do dia da data atual." está na tela
	And Eu clico no SubMenu "Reservas" na tela "Tela Reserva"
	And Eu informo o produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Eu pesquiso as reservas
	And Eu realizo a pesquisa
	And Eu apago a reserva do produto selecionado
	When Confirmo a remoção
	Then Fecho o alerta

@E001_Acertos
Scenario: Validar vencimento de parcela E001 na data base do mês futuro. (acerto)
	And Eu Clico no botão "Nova Reserva"
	And Eu Clico no botão "Nova Reserva"
	And Eu informo a data base "05/06/2017"
	And Informo o empreendimento "PARQUE SERRA AZUL"
	And Informo o Produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Informo o cliente "TESTE AUTOMATIZADOS"
	And Seleciono o atendimento correspondente ao cliente
	And Informo o Tipo de Financiamento "CEF ASSOCIATIVO P IPCA +1% (D90)"
	And Seleciono o Banco de Desligamento do Cliente "CAIXA ECONÔMICA FEDERAL"
	And Eu Clico no botão "Salvar"
	And Confirmo a geração da reserva
	And Exibe a mensagem "Reserva gerada com sucesso!"
	And Eu clico no botão "Gerar Proposta"
	And Informo uma Renda Presumida de 15000,00
	And Informo o valor da tabela de vendas de financiamento como financiamento
	And Informo um Valor de Sinal de "10000" Tipo de pagamento "BL - Boleto" data de vencimento do sinal ""
	And Valido se não tem a mensagem "Ato (E001) - Vencimento fora do período permitido. Favor selecionar um vencimento de 2 dia(s) a partir do dia da data atual e mês/ano da data base do contrato." na tela
	And Eu clico no SubMenu "Reservas" na tela "Tela Reserva"
	And Eu informo o produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Eu pesquiso as reservas
	And Eu realizo a pesquisa
	And Eu apago a reserva do produto selecionado
	When Confirmo a remoção
	Then Fecho o alerta

Scenario: Validar vencimento de parcela E001 na data base do mes atual. (acerto)
	Given Eu Clico no botão "Nova Reserva"
	And Eu Clico no botão "Nova Reserva"
	And Eu informo a data base ""
	And Informo o empreendimento "PARQUE SERRA AZUL"
	And Informo o Produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Informo o cliente "TESTE AUTOMATIZADOS"
	And Seleciono o atendimento correspondente ao cliente
	And Informo o Tipo de Financiamento "CEF ASSOCIATIVO P IPCA +1% (D90)"
	And Seleciono o Banco de Desligamento do Cliente "CAIXA ECONÔMICA FEDERAL"
	And Eu Clico no botão "Salvar"
	And Confirmo a geração da reserva
	And Exibe a mensagem "Reserva gerada com sucesso!"
	And Eu clico no botão "Gerar Proposta"
	And Informo uma Renda Presumida de 15000,00
	And Informo o valor da tabela de vendas de financiamento como financiamento
	And Informo um Valor de Sinal de "10000" Tipo de pagamento "BL - Boleto" data de vencimento do sinal ""
	And Valido se não tem a mensagem "Ato (E001) - Vencimento fora do período permitido. Favor selecionar um vencimento de 2 dia(s) a partir do dia da data atual." na tela
	And Eu clico no SubMenu "Reservas" na tela "Tela Reserva"
	And Eu informo o produto "PARQUE SERRA AZUL - BLOCO 17 - 2 Q - APTO 402"
	And Eu pesquiso as reservas
	And Eu realizo a pesquisa
	And Eu apago a reserva do produto selecionado
	When Confirmo a remoção
	Then Fecho o alerta