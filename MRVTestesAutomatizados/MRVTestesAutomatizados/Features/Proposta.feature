﻿Feature: Proposta
	Com intuito de evitar impactos na geração de proposta e testar a funcionalidade 
	eu como usuário dos dos sitemas comerciais 
	desejo realizar o ganho de uma proposta de uma reserva.

Background: 
	Given Eu preencho o usuário com "ademarrosa"
	And Eu preencho a senha com "ademar@1234"
	And Eu Clico no botão "Entrar"
	And Eu clico no menu "MRVCorretor"
	And Eu clico no SubMenu "Reservas"
	And Eu Clico no botão "Nova Reserva"
	And Informo a data base do mês atual
	And Informo o empreendimento "PARQUE SINTONIA"
	And Informo o Produto "PARQUE SINTONIA - BLOCO 10 - 2 Q - APTO 402"
	And Informo o cliente "TESTE AUTOMATIZADOS"
	And Seleciono o atendimento correspondente ao cliente
	And Informo o Tipo de Financiamento "CEF ASSOCIATIVO P IPCA +1% (D90)"
	And Seleciono o Banco de Desligamento do Cliente "CAIXA ECONÔMICA FEDERAL"
	And Eu Clico no botão "Salvar"
	And Confirmo a geração da reserva
	And Exibe a mensagem "Reserva gerada com sucesso!"

Scenario: Ganhar proposta
	Given  Eu clico no botão "Gerar Proposta"
	And Informo uma Renda Presumida de 15000,00
	And Informo o valor da tabela de vendas de financiamento como financiamento
	And Informo um Sinal de 10000,00
	And Informo o Saldo a Distribuir como FGTS 
	And Informo a forma de pagamento do Registro do contrato como "À Vista"
	And Eu Clico no botão "Ganhar"
	When Eu clico no botão "Confirmar"
	Then O sitema exibe a mensagem "Proposta ganha com sucesso."
