﻿Feature: Parcela_E001_Passado
	Com intuito de evitar impactos nas regras de vencimento do sinal e testar a funcionalidade 
	eu como usuário dos dos sitemas comerciais 
	desejo informar diferentes formas de vencimento de sinais.

@E001_Passada_Errado
Scenario: Validar vencimento de parcela E001 na data base do mês passado. (erro)
	Given Fazer login "ademarrosa" e "ademar@1234"
	And Eu clico no menu "MRVCorretor"
	And Eu clico no SubMenu "Reservas" na tela "Tela principal"
	And Eu pesquiso pelo status "Pendente Aprovação"
	And Eu Clico no botão "Pesquisar"
	And Eu gero a proposta
	And Informo uma Renda Presumida de 15000,00
	And Informo o valor da tabela de vendas de financiamento como financiamento
	And Informo um Valor de Sinal de "10000" Tipo de pagamento "BL - Boleto" data de vencimento do sinal "06/17/2017"
	Then Valido se a mensagem "Ato (E001) - Vencimento fora do período permitido. Favor selecionar um vencimento de 3 dia(s) a partir do dia da data atual." está na tela

@E001_Passada_Certo
Scenario: Validar vencimento de parcela E001 na data base do mês passado. (Certo)
	And Eu clico no SubMenu "Reservas" na tela "Tela Reserva"
	And Eu pesquiso pelo status "Pendente Aprovação"
	And Eu Clico no botão "Pesquisar"
	And Eu gero a proposta
	And Informo uma Renda Presumida de 15000,00
	And Informo o valor da tabela de vendas de financiamento como financiamento
	#Preencher a data de vencimento do sinal com a mesma data que está no config do servidor
	And Informo um Valor de Sinal de "10000" Tipo de pagamento "BL - Boleto" data de vencimento do sinal "05/15/2017"
	Then Valido se não tem a mensagem "Ato (E001) - Vencimento fora do período permitido. Favor selecionar um vencimento de 2 dia(s) a partir do dia da data atual." na tela
