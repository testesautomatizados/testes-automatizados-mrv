﻿Feature: Reserva
	Com intuito de evitar impactos na geração de reserva e testar a funcionalidade 
	eu como usuário dos dos sitemas comerciais 
	desejo realizar uma reserva de um produto MRV

#	Examples: 
#
#	Tipos de plano 
#		FLEX À VISTA
#		FLEX À VISTA C/ FGTS
#		FLEX FIXO PRAZO DE OBRA
#		FLEX FIXO PRAZO DE OBRA C/FGTS
#		CEF ASSOCIATIVO P IPCA +1% (D90)
#		FLEX FIXO PRAZO DE OBRA C/FGTS (D90)
#		CEF ASSOCIATIVO P IPCA +1%

Background: 
Given Eu preencho o usuário com "ademarrosa"
And Eu preencho a senha com "ademar@1234"
And Eu Clico no botão "Entrar"
And Eu clico no menu "MRVCorretor"
And Eu clico no SubMenu "Reservas"

@CriacaoReserva
Scenario: Gerar uma reserva
	Given Eu Clico no botão "Nova Reserva"
	And Informo a data base do mês atual
	And Informo o empreendimento "Franca Garden"
	And Informo o Produto "FRANCA GARDEN - BLOCO 27 - 2 Q - APTO 303"
	And Informo o cliente "TESTE AUTOMATIZADOS"
	And Seleciono o atendimento correspondente ao cliente
	And Informo o Tipo de Financiamento "CEF ASSOCIATIVO P IPCA +1% (D90)"
	And Seleciono o Banco de Desligamento do Cliente "CAIXA ECONÔMICA FEDERAL"
	And Eu Clico no botão "Salvar"
	When Confirmo a geração da reserva
	Then Exibe a mensagem "Reserva gerada com sucesso!"

