﻿Feature: Login
	Na intenção de testar o acesso ao sistemas comerciais 
	e evitar enganos 

@Login
Scenario: Acessar ao sistema comercial com o login de usuário corretor. 
	Given Eu preencho o usuário com "veruska.assis"
	And Eu preencho a senha com "veruska@456"
	When Eu pressiono o botão entrar
	Then Estou logado no sistema comercial
