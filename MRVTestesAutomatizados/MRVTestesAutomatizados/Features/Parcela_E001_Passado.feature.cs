﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.1.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace MRVTestesAutomatizados.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [TechTalk.SpecRun.FeatureAttribute("Parcela_E001_Passado", Description="\tCom intuito de evitar impactos nas regras de vencimento do sinal e testar a func" +
        "ionalidade \r\n\teu como usuário dos dos sitemas comerciais \r\n\tdesejo informar dife" +
        "rentes formas de vencimento de sinais.", SourceFile="Features\\Parcela_E001_Passado.feature", SourceLine=0)]
    public partial class Parcela_E001_PassadoFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "Parcela_E001_Passado.feature"
#line hidden
        
        [TechTalk.SpecRun.FeatureInitialize()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Parcela_E001_Passado", "\tCom intuito de evitar impactos nas regras de vencimento do sinal e testar a func" +
                    "ionalidade \r\n\teu como usuário dos dos sitemas comerciais \r\n\tdesejo informar dife" +
                    "rentes formas de vencimento de sinais.", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [TechTalk.SpecRun.FeatureCleanup()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        [TechTalk.SpecRun.ScenarioCleanup()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Validar vencimento de parcela E001 na data base do mês passado. (erro)", new string[] {
                "E001_Passada_Errado"}, SourceLine=6)]
        public virtual void ValidarVencimentoDeParcelaE001NaDataBaseDoMesPassado_Erro()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Validar vencimento de parcela E001 na data base do mês passado. (erro)", new string[] {
                        "E001_Passada_Errado"});
#line 7
this.ScenarioSetup(scenarioInfo);
#line 8
 testRunner.Given("Fazer login \"ademarrosa\" e \"ademar@1234\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 9
 testRunner.And("Eu clico no menu \"MRVCorretor\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 10
 testRunner.And("Eu clico no SubMenu \"Reservas\" na tela \"Tela principal\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 11
 testRunner.And("Eu pesquiso pelo status \"Pendente Aprovação\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 12
 testRunner.And("Eu Clico no botão \"Pesquisar\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 13
 testRunner.And("Eu gero a proposta", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 14
 testRunner.And("Informo uma Renda Presumida de 15000,00", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 15
 testRunner.And("Informo o valor da tabela de vendas de financiamento como financiamento", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 16
 testRunner.And("Informo um Valor de Sinal de \"10000\" Tipo de pagamento \"BL - Boleto\" data de venc" +
                    "imento do sinal \"06/17/2017\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 17
 testRunner.Then("Valido se a mensagem \"Ato (E001) - Vencimento fora do período permitido. Favor se" +
                    "lecionar um vencimento de 3 dia(s) a partir do dia da data atual.\" está na tela", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Validar vencimento de parcela E001 na data base do mês passado. (Certo)", new string[] {
                "E001_Passada_Certo"}, SourceLine=19)]
        public virtual void ValidarVencimentoDeParcelaE001NaDataBaseDoMesPassado_Certo()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Validar vencimento de parcela E001 na data base do mês passado. (Certo)", new string[] {
                        "E001_Passada_Certo"});
#line 20
this.ScenarioSetup(scenarioInfo);
#line 21
 testRunner.And("Eu clico no SubMenu \"Reservas\" na tela \"Tela Reserva\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 22
 testRunner.And("Eu pesquiso pelo status \"Pendente Aprovação\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 23
 testRunner.And("Eu Clico no botão \"Pesquisar\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 24
 testRunner.And("Eu gero a proposta", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
 testRunner.And("Informo uma Renda Presumida de 15000,00", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 26
 testRunner.And("Informo o valor da tabela de vendas de financiamento como financiamento", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 28
 testRunner.And("Informo um Valor de Sinal de \"10000\" Tipo de pagamento \"BL - Boleto\" data de venc" +
                    "imento do sinal \"05/15/2017\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 29
 testRunner.Then("Valido se não tem a mensagem \"Ato (E001) - Vencimento fora do período permitido. " +
                    "Favor selecionar um vencimento de 2 dia(s) a partir do dia da data atual.\" na te" +
                    "la", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.TestRunCleanup()]
        public virtual void TestRunCleanup()
        {
            TechTalk.SpecFlow.TestRunnerManager.GetTestRunner().OnTestRunEnd();
        }
    }
}
#pragma warning restore
#endregion
