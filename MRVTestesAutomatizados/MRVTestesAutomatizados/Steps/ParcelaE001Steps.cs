﻿using MRVTestesAutomatizados.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Steps
{
    [Binding, 
     Scope(Feature = "ParcelaE001"),
     Scope(Feature = "Parcela_E001_Passado")]
    public class ParcelaE001Steps : BaseStep
    {
        private ParcelaE001Page _parcelaE001Page = new ParcelaE001Page();
        private ParcelaE001Page ParcelaPage
        {
            get
            {
                return _parcelaE001Page ?? (_parcelaE001Page = new ParcelaE001Page());
            }
        }

        [Given(@"Informo um Valor de Sinal de ""(.*)"" Tipo de pagamento ""(.*)"" data de vencimento do sinal ""(.*)""")]
        public void GivenInformoUmValorDeSinalDeTipoDePagamentoDataDeVencimentoDoSinal(double valorSinal, string tipoPagamento, string dataSinal)
        {
            DateTime dataVencimentoSinal;
            DateTime.TryParse(dataSinal, out dataVencimentoSinal);
            ParcelaPage.InformoSinalProposta(Driver, valorSinal, tipoPagamento, dataVencimentoSinal);
        }

        [Given(@"Eu informo o produto ""(.*)""")]
        public void GivenEuInformoOProduto(string nomeProduto)
        {
            ParcelaPage.InformarNomeProdutoParaCancelamento(Driver, nomeProduto);
        }

        [Given(@"Eu apago a reserva do produto selecionado")]
        public void ThenEuApagoAReservaDoProdutoSelecionado()
        {
            ParcelaPage.ApagarReserva(Driver);
        }

        [Given(@"Eu confirmo o produto")]
        public void GivenEuConfirmoOProduto()
        {
            ParcelaPage.ConfirmarProduto(Driver);
        }

        [Given(@"Eu pesquiso as reservas")]
        public void GivenEuPesquisoAsReservas()
        {
            ParcelaPage.PesquisarReservas(Driver);
        }

        [Given(@"Eu realizo a pesquisa")]
        public void GivenEuRealizoAPesquisa()
        {
            ParcelaPage.RealizarPesquisa(Driver);
        }


        [When(@"Confirmo a remoção")]
        public void ThenConfirmoARemocao()
        {
            ParcelaPage.ConfirmarExclusao(Driver);
        }

        [Given(@"Eu informo a data base ""(.*)""")]
        public void GivenInformoADataBaseDoMesAtual(string dataBase)
        {
            ParcelaPage.ConfirmarDataBase(Driver, dataBase);
        }

        [Given(@"Valido se a mensagem ""(.*)"" está na tela")]
        [Then(@"Valido se a mensagem ""(.*)"" está na tela")]
        public void GivenValidoSeAMensagemEstaNaTela(string mensagem)
        {
            ParcelaPage.ValidaExibicaoMensagem(Driver, mensagem);
        }

        [Given(@"Valido se não tem a mensagem ""(.*)"" na tela")]
        [Then(@"Valido se não tem a mensagem ""(.*)"" na tela")]
        public void GivenValidoSeNaoTemAMensagemNaTela(string mensagem)
        {
            ParcelaPage.ValidaNaoExibicaoMensagem(Driver, mensagem);
        }


        [Then(@"Fecho o alerta")]
        public void ThenFechoOAlerta()
        {
            ParcelaPage.FehcarAlerta(Driver);
        }

    }
}
