﻿using MRVTestesAutomatizados.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Steps
{
    [Binding]
    public class LoginSteps : BaseStep
    {
        private LoginPage _loginPage = new LoginPage();
        private LoginPage LoginPage
        {
            get
            {
                return _loginPage ?? (_loginPage = new LoginPage());
            }
        }

        [Given(@"Fazer login ""(.*)"" e ""(.*)""")]
        public void GivenFazerLoginE(string usuario, string senha)
        {
            LoginPage.FazerLogin(Driver, usuario, senha);
        }


        [Given(@"Eu preencho o usuário com ""(.*)""")]
        public void GivenEuPreenchoOUsuarioCom(string usuario)
        {
            LoginPage.PreencherTextBoxUsuario(Driver, usuario);
        }
        
        [Given(@"Eu preencho a senha com ""(.*)""")]
        public void GivenEuPreenchoASenhaCom(string senha)
        {
            LoginPage.PreencherTextBoxSenha(Driver, senha);
        }
        
        [When(@"Eu pressiono o botão entrar")]
        public void WhenEuPressionoOBotaoEntrar()
        {
            LoginPage.ClicarBotaoEntrar(Driver);
        }
        
        [Then(@"Estou logado no sistema comercial")]
        public void ThenEstouLogadoNoSistemaComercial()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
