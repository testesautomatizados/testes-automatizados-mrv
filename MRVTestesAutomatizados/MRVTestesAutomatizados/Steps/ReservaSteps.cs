﻿using MRVTestesAutomatizados.Enums;
using MRVTestesAutomatizados.Pages;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Steps
{
    [Binding]
    public class ReservaSteps : BaseStep
    {
        private ReservaPage _reservaPage = new ReservaPage();
        private ReservaPage ReservaPage
        {
            get
            {
                return _reservaPage ?? (_reservaPage = new ReservaPage());
            }
        }


        [Given(@"Informo a data base do mês atual")]
        public void GivenInformoADataBaseDoMesAtual()
        {
            ReservaPage.ConfirmarDataBase(Driver);
        }
        
        [Given(@"Informo o empreendimento ""(.*)""")]
        public void GivenInformoOEmpreendimento(string empreendimento)
        {
            ReservaPage.InformarEmpreendimento(Driver, empreendimento);
            ReservaPage.SelecionarEmpreendimento(Driver);
            ReservaPage.VincularEmpreendimento(Driver);
        }

        [Given(@"Eu Clico no botão ""(.*)""")]
        public void GivenEuClicoNoBotao(string botao)
        {
            ReservaPage.ClicoBotaoDescricao(Driver, botao);
        }

        [Given(@"Informo o Produto ""(.*)""")]
        public void GivenInformoOProduto(string produto)
        {
            ReservaPage.InformarProduto(Driver, produto);
            TempoReacao();

            ReservaPage.PesquisarProduto(Driver, produto);
            TempoReacao();

            ReservaPage.VincularProduto(Driver);
            TempoReacao();
        }

        [Given(@"Informo o cliente ""(.*)""")]
        public void GivenInformoOCliente(string cliente)
        {
            ReservaPage.InformarCliente(Driver, cliente);
            TempoReacao();

            ReservaPage.PesquisarCliente(Driver);
            TempoReacao();

            ReservaPage.VincularCliente(Driver);
            TempoReacao();

        }

        [Given(@"Seleciono o atendimento correspondente ao cliente")]
        public void GivenSelecionoOAtendimentoCorrespondenteAoCliente()
        {
            ReservaPage.VincularAtendimentoCliente(Driver);
        }
        
        [Given(@"Informo o Tipo de Financiamento ""(.*)""")]
        public void GivenInformoOTipoDeFinanciamento(string tipoFinanciamento)
        {
            ReservaPage.InformarTipoFinanciamento(Driver, tipoFinanciamento);
        }
        
        [Given(@"Seleciono o Banco de Desligamento do Cliente ""(.*)""")]
        public void GivenSelecionoOBancoDeDesligamentoDoCliente(string bancoDesligamento)
        {
            ReservaPage.InformarBancoDesligamento(Driver, bancoDesligamento);
        }
        
        [When(@"Confirmo a geração da reserva")]
        [Given(@"Confirmo a geração da reserva")]
        public void ConfirmoAGeracaoDaReserva()
        {
            ReservaPage.ConfirmarGeracaoReserva(Driver);
        }
        
        [Then(@"Exibe a mensagem ""(.*)""")]
        public void ThenExibeAMensagem(string mensagem)
        {
            ReservaPage.ValidaExibicaoMensagem(Driver, mensagem);
            ReservaPage.FecharCaixaMensagem(Driver);
        }

        
    }
}
