﻿using MRVTestesAutomatizados.Enums;
using MRVTestesAutomatizados.Pages;
using System;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Steps
{
    [Binding]
    public class NavegacaoMenusSteps : BaseStep
    {
        private MenuPage _menuPage = new MenuPage(Driver);
        private MenuPage MenuPage
        {
            get
            {
                return _menuPage ?? (_menuPage = new MenuPage(Driver));
            }
        }

        [Given(@"Eu clico no menu ""(.*)""")]
        public void GivenEuClicoNoMenu(string menu)
        {
            switch (menu.ToLower())
            {
                case "corretor":
                case "mrvcorretor":
                    MenuPage.SelecionarMenuSistema(Sistemas.MenuSistema.Corretor);
                    break;
                default:
                    break;
            }
            
        }

        [Given(@"Eu clico no SubMenu ""(.*)"" na tela ""(.*)""")]
        public void GivenEuClicoNoSubMenu(string subMenu, string tela)
        {
            switch (subMenu.ToLower())
            {
                case "reservas":
                case "mrvreservas":
                    MenuPage.SelecionarSubMenu(Sistemas.SubMenuCorretor.Reservas, tela);
                    break;
            }
        }

    }
}
