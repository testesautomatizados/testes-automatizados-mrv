﻿using MRVTestesAutomatizados.Enums;
using MRVTestesAutomatizados.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Steps
{
    [Binding]
    public abstract class BaseStep
    {

        #region Driver
        private static IWebDriver _driver = BuscarDriver();
        public static IWebDriver Driver
        {
            get
            {
                return _driver ?? BuscarDriver();
            }
        }

        [BeforeFeature]
        public static void InitalizeDriver()
        {
            Driver.Navigate().GoToUrl(Ambientes.URLs.MRVComercialH);
        }

        private static IWebDriver BuscarDriver()
        {
            DesiredCapabilities desiredCapabilities = DesiredCapabilities.Chrome();
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("incognito");
            desiredCapabilities.SetCapability(ChromeOptions.Capability, options);
            return new ChromeDriver(@"C:\Users\samir\Desktop\MRV\Teste", options);
        }
        #endregion

        [BeforeStep]
        public static void TempoReacao()
        {
            Thread.Sleep(TimeSpan.FromSeconds(1));
        }

        [AfterFeature]
        public static void FecharNavegador()
        {
            Driver.Dispose();
        }
    }
}
