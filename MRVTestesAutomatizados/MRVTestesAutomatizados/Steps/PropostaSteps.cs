﻿using MRVTestesAutomatizados.Pages;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Steps
{
    [Binding]
    public class PropostaSteps : BaseStep
    {
        private PropostaPage _propostaPage = new PropostaPage();
        private PropostaPage PropostaPage
        {
            get
            {
                return _propostaPage ?? (_propostaPage = new PropostaPage());
            }
        }

        [Given(@"Eu clico no botão ""(.*)""")]
        [When(@"Eu clico no botão ""(.*)""")]
        public void GivenEuClicoNoBotao(string descricaoBotao)
        {
            PropostaPage.ClicoBotaoDescricao(Driver, descricaoBotao);
        }

        [Given(@"Informo uma Renda Presumida de (.*)")]
        public void GivenInformoUmaRendaPresumidaDe(double rendaPresumida)
        {
            PropostaPage.InformarRendaPresumidaProposta(Driver, rendaPresumida);
        }
        
        [Given(@"Informo o valor da tabela de vendas de financiamento como financiamento")]
        public void GivenInformoOValorDaTabelaDeVendasDeFinanciamentoComoFinanciamento()
        {
            PropostaPage.InformoValorTabelaVendasFinanciamentoComoFinanciamentoProposta(Driver);
        }
        
        [Given(@"Informo um Sinal de (.*)")]
        public void GivenInformoUmSinalDe(double valorSinal)
        {
            PropostaPage.InformoSinalProposta(Driver, valorSinal);
        }

        [Given(@"Informo o Saldo a Distribuir como FGTS")]
        public void GivenInformoOSaldoADistribuirComoFGTS()
        {
            PropostaPage.InformoSaldoDistribuirFgst(Driver);
        }
        
        [Given(@"Informo a forma de pagamento do Registro do contrato como ""(.*)""")]
        public void GivenInformoAFormaDePagamentoDoRegistroDoContratoComo(string formaPagamentoRegistro)
        {
            PropostaPage.InformoFormaPagamentoRegistroContrato(Driver, formaPagamentoRegistro);
        }

        [Given(@"O sitema exibe a mensagem ""(.*)""")]
        [Then(@"O sitema exibe a mensagem ""(.*)""")]
        public void ThenOSitemaExibeAMensagem(string mensagem)
        {
            PropostaPage.ValidaExibicaoMensagem(Driver, mensagem);
        }

        [Given(@"Exibe a mensagem ""(.*)""")]
        public void ExibeMensagem(string mensagem)
        {
            PropostaPage.ValidaExibicaoMensagem(Driver);
        }
    }
}
