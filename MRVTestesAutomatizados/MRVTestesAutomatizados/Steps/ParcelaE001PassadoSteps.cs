﻿using MRVTestesAutomatizados.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace MRVTestesAutomatizados.Steps
{
    public class ParcelaE001PassadoSteps : BaseStep
    {

        private ParcelaE001PassadoPage _parcelaE001PassadoPage = new ParcelaE001PassadoPage();
        private ParcelaE001PassadoPage ParcelaPassadoPage
        {
            get
            {
                return _parcelaE001PassadoPage ?? (_parcelaE001PassadoPage = new ParcelaE001PassadoPage());
            }
        }

        [Given(@"Eu pesquiso pelo status ""(.*)""")]
        public void GivenEuPesquisoPeloStatus(string status)
        {
            ParcelaPassadoPage.PesquisarStatus(Driver, status);
        }

        [Given(@"Eu gero a proposta")]
        public void GivenEuGeroAProposta()
        {
            ParcelaPassadoPage.GerarProposta(Driver);
        }
    }
}
