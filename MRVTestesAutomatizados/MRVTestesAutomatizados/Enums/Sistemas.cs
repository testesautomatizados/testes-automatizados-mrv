﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Enums
{
    public class Sistemas
    {
        public enum MenuSistema
        {
            Corretor,
            Validador,
            Prospect,
            Noticias,
            Configuracoes

        }

        public enum SubMenuCorretor
        {
            Reservas,
            Cliente,
            Credito,
            LinksUteis,
            Relatorios,
            PagamentoRPA
        }
    }
}
