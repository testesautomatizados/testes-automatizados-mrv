﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Enums
{
    public class Ambientes
    {
        public struct URLs
        {
            public const string MRVComercialD = "http://mrvcomerciald.mrv.com.br";
            public const string MRVComercialH = "http://mrvcomercialh.mrv.com.br";
            public const string MRVCorretorH = "http://mrvcorretorh.mrv.com.br";
        }
    }
}
