﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRVTestesAutomatizados.Enums
{
    public class Seletores
    {
        public enum SeletorBy
        {
            ByClassName,
            ByCssSelector,
            ById,
            ByLinkText,
            ByName,
            ByPartialLinkText,
            ByTagName,
            ByXPath
        }

        public enum SeletorComboBox
        {
            ByIndex,
            ByText,
            ByValue
        }
    }
}
